// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription
import CompilerPluginSupport

let swiftGodotCoreUrl = "https://gitlab.com/indexing-your-heart/engine/SwiftGodotCore/"
let swiftGodotCoreTag = "1.0.0-alpha23-DEVELOPMENT-SNAPSHOT-2023-10-28-b"
let swiftGodotCheksum = "7c2a5a96db0d0a118fbed8605e034c845636eac8319eb8d875488109f50cba37"

let macroContent: [Target] = [
    .macro(
        name: "SwiftGodotMacroLibrary",
        dependencies: [
            .product(name: "SwiftSyntaxMacros", package: "swift-syntax"),
            .product(name: "SwiftCompilerPlugin", package: "swift-syntax")
        ]
    ),
    .target(name: "SwiftGodotMacros",
            dependencies: ["SwiftGodotMacroLibrary", .target(name: "SwiftGodotCore")]),
    .executableTarget(name: "SwiftGodotMacrosClient", dependencies: ["SwiftGodot"]),
    .testTarget(
        name: "SwiftGodotMacrosTests",
        dependencies: [
            "SwiftGodotMacroLibrary",
            .product(name: "SwiftSyntaxMacrosTestSupport", package: "swift-syntax"),
        ]
    ),
]


let package = Package(
    name: "SwiftGodot",
    platforms: [.macOS(.v13), .iOS(.v16)],
    products: [
        .library(name: "SwiftGodot",
                 targets: ["SwiftGodot"]),
        .library(
            name: "SwiftGodotCore",
            targets: ["SwiftGodotCore"]),
        .library(name: "SwiftGodotLogging",
                 targets: ["SwiftGodotLogging"]),
        .library(name: "SwiftGodotInspection", targets: ["SwiftGodotInspection"]),
        .library(
            name: "SwiftGodotMacros",
            targets: ["SwiftGodotMacros"]),
        .executable(
            name: "SwiftGodotMacrosClient",
            targets: ["SwiftGodotMacrosClient"]),
    ],
    dependencies: [
        .package(url: "https://github.com/apple/swift-syntax.git", from: "509.0.0"),
        .package(url: "https://github.com/apple/swift-log.git", from: "1.0.0")
    ],
    targets: [
        .target(name: "SwiftGodot",
                dependencies: ["SwiftGodotCore", "SwiftGodotMacros", "SwiftGodotLogging", "SwiftGodotInspection"]),
        .binaryTarget(
            name: "SwiftGodotCore",
            url: "\(swiftGodotCoreUrl)-/releases/\(swiftGodotCoreTag)/downloads/SwiftGodotCore.xcframework.zip",
            checksum: swiftGodotCheksum),
        .target(name: "SwiftGodotLogging",
                dependencies: ["SwiftGodotCore", .product(name: "Logging", package: "swift-log")]),
        .target(name: "SwiftGodotInspection",
                dependencies: ["SwiftGodotCore"]),
        .testTarget(name: "SwiftGodotInspectionTests",
                    dependencies: ["SwiftGodotInspection"])
    ] + macroContent
)
