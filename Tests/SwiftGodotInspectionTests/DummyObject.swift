//
//  File.swift
//  
//
//  Created by Marquis Kurt on 10/17/23.
//

import SwiftGodotCore

class DummyObject: Object {
    enum Fruit: Int, CaseIterable, Nameable {
        case apple
        case banana

        var name: String {
            switch self {
            case .apple:
                "Apple"
            case .banana:
                "Banana"
            }
        }
    }

    static var autoProperty: InspectableProperty<DummyObject> {
        .init(DummyObject.self, getter: DummyObject.propertyGetterSetter, setter: DummyObject.propertyGetterSetter)
    }

    func propertyGetterSetter(args: [Variant]) -> Variant? { nil }
}
