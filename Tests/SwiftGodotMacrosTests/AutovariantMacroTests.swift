//
//  AutovariantMacroTests.swift
//
//
//  Created by Marquis Kurt on 10/13/23.
//

import SwiftSyntaxMacros
import SwiftSyntaxMacrosTestSupport
import XCTest
import SwiftGodotMacroLibrary

class AutovariantMacroTests: XCTestCase {
    let testMacros: [String: Macro.Type] = [
        "Autovariant": AutovariantMacro.self
    ]

    func testMacroExpansionWithTypeAnnotation() throws {
        assertMacroExpansion(
            """
            public class MyNode {
                @Autovariant public var exposed: Bool = false
            }
            """,
            expandedSource:
                """
                public class MyNode {
                    public var exposed: Bool = false

                    /// A wrapper for getting ``exposed`` that can be exposed to Godot for registration.
                    ///
                    /// - Note: It is not recommended to call this method directly.
                    public func _getVariant_exposed(args: [Variant]) -> Variant? {
                        Variant(exposed)
                    }

                    /// A wrapper for setting ``exposed`` that can be exposed to Godot for registration.
                    ///
                    /// - Note: It is not recommended to call this method directly.
                    public func _setVariant_exposed(args: [Variant]) -> Variant? {
                        ClassInfo.withCheckedProperty(named: "exposed", in: args) { arg in
                            if let realValue = Bool(arg) {
                                exposed = realValue
                            }
                        }
                    }
                }
                """,
            macros: testMacros)
    }
}
