//
//  File.swift
//  
//
//  Created by Marquis Kurt on 10/6/23.
//

import SwiftSyntaxMacros
import SwiftSyntaxMacrosTestSupport
import XCTest
import SwiftGodotMacroLibrary

class CallableMacroTests: XCTestCase {
    let testMacros: [String: Macro.Type] = [
        "Callable": CallableMacro.self
    ]

    func testGodotMainMacroExpansion() throws {
        assertMacroExpansion(
            """
            class Castro: Node {
                @Callable private func deleteEpisode() {
                }
            }
            """,
            expandedSource: """
                            class Castro: Node {
                                private func deleteEpisode() {
                                }

                                /// A wrapper for the ``deleteEpisode`` method can can be exposed to Godot through registration.
                                ///
                                /// - Note: It is recommended to not call this method directly.
                                private func _callable_deleteEpisode(args: [Variant]) -> Variant? {
                                    deleteEpisode()
                                    return nil
                                }
                            }
                            """,
            macros: testMacros)
    }
}
