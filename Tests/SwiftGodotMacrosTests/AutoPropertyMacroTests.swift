//
//  File.swift
//  
//
//  Created by Marquis Kurt on 10/15/23.
//

import SwiftSyntaxMacros
import SwiftSyntaxMacrosTestSupport
import XCTest
import SwiftGodotMacroLibrary

class AutoPropertyMacroTests: XCTestCase {
    let testMacros: [String: Macro.Type] = [
        "autoProperty": AutoPropertyMacro.self,
        "Autovariant": AutovariantMacro.self
    ]

    func testGodotMainMacroExpansion() throws {
        assertMacroExpansion(
            """
            class Castro: Node {
                @Autovariant var exposed: Bool = false

                func hello() {
                    let toast = #autoProperty(object: Castro.self, "exposed")
                }
            }
            """,
            expandedSource: """
                            class Castro: Node {
                                var exposed: Bool = false
                            
                                /// A wrapper for getting ``exposed`` that can be exposed to Godot for registration.
                                ///
                                /// - Note: It is not recommended to call this method directly.
                                func _getVariant_exposed(args: [Variant]) -> Variant? {
                                    Variant(exposed)
                                }

                                /// A wrapper for setting ``exposed`` that can be exposed to Godot for registration.
                                ///
                                /// - Note: It is not recommended to call this method directly.
                                func _setVariant_exposed(args: [Variant]) -> Variant? {
                                    ClassInfo.withCheckedProperty(named: "exposed", in: args) { arg in
                                        if let realValue = Bool(arg) {
                                            exposed = realValue
                                        }
                                    }
                                }

                                func hello() {
                                    let toast =         InspectableProperty(Castro.self, getter: Castro._getVariant_exposed, setter: Castro._setVariant_exposed)
                                }
                            }
                            """,
            macros: testMacros)
    }
}
