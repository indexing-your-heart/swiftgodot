//
//  AutovariantEnumMacroTests.swift
//
//
//  Created by Marquis Kurt on 10/15/23.
//

import SwiftSyntaxMacros
import SwiftSyntaxMacrosTestSupport
import XCTest
import SwiftGodotMacroLibrary

class AutovariantEnumMacroTests: XCTestCase {
    let testMacros: [String: Macro.Type] = [
        "AutovariantEnum": AutovariantEnumMacro.self
    ]

    func testMacroExpansionWithTypeAnnotation() throws {
        assertMacroExpansion(
            """
            public class MyNode {
                @PickerNameProvider
                public enum Fruit: Int {
                    case apple
                    case banana
                }
                @AutovariantEnum public var fruit: Fruit = .apple
            }
            """,
            expandedSource:
                """
                public class MyNode {
                    @PickerNameProvider
                    public enum Fruit: Int {
                        case apple
                        case banana
                    }
                    public var fruit: Fruit = .apple

                    /// A wrapper for getting ``fruit`` that can be exposed to Godot for registration.
                    ///
                    /// - Note: It is not recommended to call this method directly.
                    public func _getVariant_fruit(args: [Variant]) -> Variant? {
                        Variant(fruit.rawValue)
                    }

                    /// A wrapper for setting ``fruit`` that can be exposed to Godot for registration.
                    ///
                    /// - Note: It is not recommended to call this method directly.
                    public func _setVariant_fruit(args: [Variant]) -> Variant? {
                        ClassInfo.withCheckedProperty(named: "fruit", in: args) { arg in
                            if let realValue = Int(arg), let enumProperty = Fruit(rawValue: realValue) {
                                fruit = enumProperty
                            }
                        }
                    }
                }
                """,
            macros: testMacros)
    }
}
