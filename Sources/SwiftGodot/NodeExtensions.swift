//
//  NodeExtensions.swift
//
//
//  Created by Marquis Kurt on 10/13/23.
//

import SwiftGodotCore

public extension Node {
    /// Returns whether a node matches as specific type.
    ///
    /// This can be used in the event `is` fails to work normally:
    ///
    /// ```swift
    /// if myNode.is(CharacterBody2D.self)
    /// ```
    /// - Parameter typeValue: The type to check against.
    func `is`<T: Object>(_ typeValue: T.Type) -> Bool {
        self.isClass("\(T.self)")
    }

    /// Adds a listener to a specified signal, if that connection has not been established.
    ///
    /// If an error occurs when connecting the signal, a ``GodotError`` will be thrown.
    /// - Parameter method: The listener to connect to a specified signal.
    /// - Parameter signal: The signal that the listener will listen for.
    @available(*, deprecated, message: "Use the standard connect(signal:,callable:) method instead.")
    func connectIfPresent(_ method: Callable, to signal: StringName) throws {
        if self.isConnected(signal: signal, callable: method) { return }
        try self.connect(signal: signal, callable: method)
    }

    /// Retrieves a property of a node that cannot be generated through source.
    ///
    /// This can be used to fetch specific properties, such as animation parameters:
    /// ```swift
    /// import SwiftGodot
    ///
    /// class MyNode {
    ///     var animationTree: AnimationTree?
    ///
    ///     override func _ready() {
    ///         let animationState = animationTree["parameters/playback"].asObject()
    ///     }
    /// }
    /// ```
    ///
    /// - Parameter property: The property to target.
    subscript(property: StringName) -> Variant {
        get {
            self.get(property: property)
        }

        set {
            self.set(property: property, value: newValue)
        }
    }
}
