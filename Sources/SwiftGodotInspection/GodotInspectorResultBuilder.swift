//
//  File.swift
//
//
//  Created by Marquis Kurt on 10/15/23.
//

import Foundation
import SwiftGodotCore
import UniformTypeIdentifiers

@_documentation(visibility: internal)
public enum InspectorNode<Parent: Object> {
    public typealias PickerType = CaseIterable & RawRepresentable<Int> & Nameable
    public struct PropertyName: Equatable {
        var name: String
        var prefix: String
    }
    case checkbox(PropertyName, property: InspectableProperty<Parent>)
    case filePicker(PropertyName, allowedTypes: [UTType], property: InspectableProperty<Parent>)
    case nodePathPicker(PropertyName, property: InspectableProperty<Parent>)
    case group(PropertyName, children: [InspectorNode<Parent>])
    case picker(PropertyName, option: any PickerType.Type, property: InspectableProperty<Parent>)
    case range(PropertyName, range: ClosedRange<Int>, stride: Int, property: InspectableProperty<Parent>)
    case textView(PropertyName, property: InspectableProperty<Parent>)
    case textField(PropertyName, property: InspectableProperty<Parent>)
}

@resultBuilder
public enum GodotInspectorResultBuilder<Parent: Object> {
    public typealias Block = GodotInspector<Parent>
    public typealias BResult = [InspectorNode<Parent>]

    public static func buildBlock<C0: Block>(_ c0: C0) -> BResult {
        [c0.build(prefix: "")]
    }

    public static func buildBlock<C0: Block, C1: Block>(_ c0: C0, _ c1: C1) -> BResult {
        [c0.build(prefix: ""), c1.build(prefix: "")]
    }

    public static func buildBlock<C0: Block, C1: Block, C2: Block>(_ c0: C0, _ c1: C1, _ c2: C2) -> BResult {
        [c0.build(prefix: ""), c1.build(prefix: ""), c2.build(prefix: "")]
    }

    public static func buildBlock<C0: Block, C1: Block, C2: Block, C3: Block>(
        _ c0: C0, _ c1: C1, _ c2: C2, _ c3: C3
    ) -> BResult {
        [c0.build(prefix: ""), c1.build(prefix: ""), c2.build(prefix: ""), c3.build(prefix: "")]
    }

    public static func buildBlock<C0: Block, C1: Block, C2: Block, C3: Block, C4: Block>(
        _ c0: C0, _ c1: C1, _ c2: C2, _ c3: C3, _ c4: C4
    ) -> BResult {
        [c0.build(prefix: ""), c1.build(prefix: ""), c2.build(prefix: ""), c3.build(prefix: ""), c4.build(prefix: "")]
    }

    public static func buildBlock<C0: Block, C1: Block, C2: Block, C3: Block, C4: Block, C5: Block>(
        _ c0: C0, _ c1: C1, _ c2: C2, _ c3: C3, _ c4: C4, _ c5: C5
    ) -> BResult {
        [
            c0.build(prefix: ""),
            c1.build(prefix: ""),
            c2.build(prefix: ""),
            c3.build(prefix: ""),
            c4.build(prefix: ""),
            c5.build(prefix: "")
        ]
    }

    public static func buildBlock<C0: Block, C1: Block, C2: Block, C3: Block, C4: Block, C5: Block, C6: Block>(
        _ c0: C0, _ c1: C1, _ c2: C2, _ c3: C3, _ c4: C4, _ c5: C5, _ c6: C6
    ) -> BResult {
        [
            c0.build(prefix: ""),
            c1.build(prefix: ""),
            c2.build(prefix: ""),
            c3.build(prefix: ""),
            c4.build(prefix: ""),
            c5.build(prefix: ""),
            c6.build(prefix: "")
        ]
    }

    public static func buildBlock<
        C0: Block,
        C1: Block,
        C2: Block,
        C3: Block,
        C4: Block,
        C5: Block,
        C6: Block,
        C7: Block>(
            _ c0: C0, _ c1: C1, _ c2: C2, _ c3: C3, _ c4: C4, _ c5: C5, _ c6: C6, _ c7: C7
        ) -> BResult {
            [
                c0.build(prefix: ""),
                c1.build(prefix: ""),
                c2.build(prefix: ""),
                c3.build(prefix: ""),
                c4.build(prefix: ""),
                c5.build(prefix: ""),
                c6.build(prefix: ""),
                c7.build(prefix: "")
            ]
        }

    public static func buildBlock<
        C0: Block,
        C1: Block,
        C2: Block,
        C3: Block,
        C4: Block,
        C5: Block,
        C6: Block,
        C7: Block,
        C8: Block>(
            _ c0: C0, _ c1: C1, _ c2: C2, _ c3: C3, _ c4: C4, _ c5: C5, _ c6: C6, _ c7: C7, _ c8: C8
        ) -> BResult {
            [
                c0.build(prefix: ""),
                c1.build(prefix: ""),
                c2.build(prefix: ""),
                c3.build(prefix: ""),
                c4.build(prefix: ""),
                c5.build(prefix: ""),
                c6.build(prefix: ""),
                c7.build(prefix: ""),
                c8.build(prefix: "")
            ]
        }

    public static func buildBlock<
        C0: Block,
        C1: Block,
        C2: Block,
        C3: Block,
        C4: Block,
        C5: Block,
        C6: Block,
        C7: Block,
        C8: Block,
        C9: Block>(
            _ c0: C0, _ c1: C1, _ c2: C2, _ c3: C3, _ c4: C4, _ c5: C5, _ c6: C6, _ c7: C7, _ c8: C8, _ c9: C9
        ) -> BResult {
            [
                c0.build(prefix: ""),
                c1.build(prefix: ""),
                c2.build(prefix: ""),
                c3.build(prefix: ""),
                c4.build(prefix: ""),
                c5.build(prefix: ""),
                c6.build(prefix: ""),
                c7.build(prefix: ""),
                c8.build(prefix: ""),
                c9.build(prefix: "")
            ]
        }
}
