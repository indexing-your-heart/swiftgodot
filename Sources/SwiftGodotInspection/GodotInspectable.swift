//
//  File.swift
//  
//
//  Created by Marquis Kurt on 10/15/23.
//

import Foundation
import SwiftGodotCore

/// A structure that contains nodes to register within Godot as inspector properties.
public struct Inspector<T: Object> {
    /// The nodes this inspector contains.
    var nodes: [InspectorNode<T>]

    /// Creates an inspector to be registered with Godot.
    /// - Parameter builder: A closure that defines the nodes to register with Godot.
    public init(@GodotInspectorResultBuilder<T> _ builder: () -> [InspectorNode<T>]) {
        self.nodes = builder()
    }
}

/// A protocol that indicates an object has inspector properties to be reigstered with Godot.
///
/// Use this to define the properties to register as Godot inspector properties:
/// ```swift
/// extension MyNode: GodotInspectable {
///     static var inspector: Inspector<MyNode> {
///         Inspector<MyNode> {
///             Checkbox("debug_mode",
///                      property: #autoProperty(MyNode, "enableDebugging")
///         }
///     }
/// }
/// ```
public protocol GodotInspectable<T>: Object {
    associatedtype T: Object
    /// The object's inspector.
    static var inspector: Inspector<T> { get }
}
