//
//  GodotLogger.swift
//  SwiftGodot
//
//  Created by Marquis Kurt on 10/4/23.
//

import SwiftGodotCore
import Logging

public struct GodotLogger: LogHandler {
    public var metadata: Logging.Logger.Metadata
    public var logLevel: Logging.Logger.Level
    var label: String

    public init(label: String) {
        self.label = label
        self.metadata = .init()
        self.logLevel = .debug
    }

    public init(metadata: Logging.Logger.Metadata, logLevel: Logging.Logger.Level) {
        self.label = "godotengine.swiftgodot.logger"
        self.metadata = metadata
        self.logLevel = logLevel
    }

    public subscript(metadataKey key: String) -> Logging.Logger.Metadata.Value? {
        get { self.metadata[key] }
        set(newValue) { self.metadata[key] = newValue }
    }

    public func log(level: Logger.Level,
                    message: Logger.Message,
                    metadata: Logger.Metadata?,
                    source: String,
                    file: String,
                    function: String,
                    line: UInt) {
        switch level {
        case .trace, .debug:
            GD.print("[\(self.label)]: \(message)")
            GD.print("╰─ At: \(file):\(line):\(function)")
        case .info, .notice:
            GD.print("[\(self.label)]: \(message)")
        case .warning:
            var newMessage = "[\(self.label)]: \(message)"
            if Engine.isEditorHint() {
                newMessage += "\n╰─ At: \(file):\(line):\(function)"
            }
            GD.pushWarning(newMessage)
        case .error, .critical:
            GD.pushError("[\(self.label)]: \(message)\n╰─ At: \(file):\(line):\(function)")
        }
    }
}
