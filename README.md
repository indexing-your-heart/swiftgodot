# SwiftGodot

**SwiftGodot** is a package that allows developers to create scripts
and extensions for the [Godot game engine](https://godotengine.org)
using the GDExtension API.

This meta package comprises of the following targets and libraries:

- **SwiftGodot** is the user-facing package that should be used and
  imported into projects. It includes all the other libraries and
  targets mentioned below, along with Foundation.
- [**SwiftGodotCore**][sgcore] is the core library that provides the
  Godot bindings. This is a fork of Miguel de Icaza's work on GitHub
  that focuses on various Apple platforms.
- **SwiftGodotLogging** is a library which provides a backend for the
  Swift Logging library (apple/swift-log) by interfacing with the
  Godot console.
- **SwiftGodotMacros** is a macro library that provides several
  macros that can be used with SwiftGodotCore to make writing scripts
  easier and more pleasant.

[sgcore]: https://gitlab.com/Indexing-Your-Heart/engine/SwiftGodotCore

## Getting started

**Required Tools**

- Xcode 15 or later
- Swift 5.9 or later

> **Important**: SwiftGodot and SwiftGodotCore currently supports
> Apple Silicon Macs and iOS devices.

### Setting up a Swift package

To start, add SwiftGodot to a Swift package as a dependency:

```swift
dependencies: [
    .package(url: "https://gitlab.com/Indexing-Your-Heart/SwiftGodot",
             branch: "root"),
    ...
]
```

Next, declare a dynamic library in your package:

```swift
products: [
    .library(name: "MyFirstGame", type: .dynamic, targets: ["MyFirstGame"]),
    ...
]
```

And, in your target, declare SwiftGodot as a dependency:

```swift
targets: [
    .target(
        name: "MyFirstGame",
        dependencies: ["SwiftGodot"],
        linkerSettings: [
            .unsafeFlags(["-Xlinker", "-undefined","-Xlinker", "dynamic_lookup"])
        ]
    ),
    ...
]
```

> :warning: **Be sure to set `linkerSettings`.**   
> These settings are required to ensure the dynamic library is built
> correctly. For more information, see the documentation for [the
> SwiftGodotCore library][sgcore-lib].

[sgcore-lib]: https://gitlab.com/indexing-your-heart/SwiftGodotCore#your-swift-code

### Scripting the extension

In your package's source files, you can write scripts that inherit
from Godot classes:

```swift
import SwiftGodot

@NativeHandleDiscarding
class SpinningCube: Node3D {
    required init() {
        super.init()
    }

    override func _ready() {
        super._ready()
        let meshRender = MeshInstance3D()
        meshRender.mesh = BoxMesh()
        addChild(meshRender)
    }

    override func _process(delta: Double) {
        rotateY(angle: delta)
    }
}
```

To make these nodes visible to Godot, a Swift extension will need to
be created. To do this, a class conforming to the 
`GodotExtensionDelegate` protocol can be created and annotated with
the `@GodotMain` macro:

```swift
import SwiftGodot

@GodotMain
class MyFirstGame: GodotExtensionDelegate {
    func extensionDidInitialize(at level: GDExtension.InitializationLevel) {
        if level == .scene {
            register(type: SampleCube.self)
        }
    }
}
```

### Getting Godot to recognize the extension

After building the extension, you'll want to get Godot to recognize
it. To do this, you'll need to define a `gdextension` file such as
below:

```ini
[configuration]
entry_symbol = "myfirstgame_entry_point"
compatibility_minimum = "4.1"

[libraries]
macos.debug = "res://bin/osx/MyFirstGame.dylib"
macos.release = "res://bin/osx/MyFirstGame.dylib"
ios.debug = "res://bin/ios/MyFirstGame.framework"
ios.release = "res://bin/ios/MyFirstGame.framework"
```

Save this `gdextension` file to your Godot project and copy over the
built libraries, either built from `swift build` or `xcodebuild`. You
will also need to copy over the `SwiftGodotCore.framework` for both
macOS and iOS, respectively. It is recommended you organize your
extensions like the following, though this isn't required*:

```
res:// (Godot root)
▿ bin
   MyFirstGame.gdextension
   ▿ osx
      MyFirstGame.dylib
      SwiftGodot.framework (Mac version)
   ▿ ios
       MyFirstGame.framework
       SwiftGodot.framework (iOS version)   
```

> \*The important part is that the paths in the `gdextension` file
> match your specified paths exactly.

### Using extensions in Godot

After opening Godot, you may get a dialog asking to reload the
project for the new extensions to take effect. Once the project is
reloaded, you can add the nodes you define in the scene tree as you
would any other Godot node.
